import UIKit
import Foundation

var currentElementNumero : String?
var currentElementTitulo = ""
//var currentElementAutor1 : String?
var currentElementSecao = ""
var currentElementAssunto = ""
var currentElementMetrica : String?
var currentElementLetra : String?
var currentElementEstrofe = ""
var currentElementEstrofeFim = ""
var currentElementVerso = ""
var currentElementCoro = ""
var currentElementCoroFim = ""
var currentElementVersoCoro = ""


var currentElementOrigem = ""
var currentElementPrimeiro = ""
var currentElementTituloOriginal = ""
var currentElementReferencia = ""
var currentElementOrigemM = ""
//var currentElementAutor2 : String?

var blnPassouUmaVeznoFoundCharacters = false

class XMLParserHelper: NSObject, XMLParserDelegate {

    var passHino = false
    var passSecao = false
    var passAssunto = false
    var passTitulo_original = false
    var passMetrica = false
    var passOrigem_letra = false
    var passPrimeiro_verso_original = false
    var passReferencia_biblica = false
    var passOrigem_musica = false

    var dictHino: [String: String] = [:]

    
    var passNumero = false
    var passTitulo = false
    var passAutor1 = false
    var passAutor2 = false
    var passEstrofe = false
    var passVerso = false
    var passCoro = false
    var passVersoCoro = false
    
    var blnVersoCoro = false
    var strVoz = ""
    
    
    func chamandoUrlXml(numero: String) {
        if let parser = XMLParser(contentsOf: URL(string:"http://novocantico.com.br/hino/\(numero.trimmingCharacters(in: .whitespaces))/\(numero.trimmingCharacters(in: .whitespaces)).xml")!) {
            
            parser.delegate = self
            parser.parse()
        }
        else {
            print("Problemas na leitura do XML")
        }
    }

    // 1
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

        if(elementName=="hino"){
            passHino = true
        }
        if(elementName=="numero"){
            passNumero = true
        }
        if(elementName=="titulo"){
            passTitulo = true
        }
        
        if(elementName=="estrofe"){
            passEstrofe = true
            currentElementVerso = ""
            blnVersoCoro = false
        }
        if(elementName=="verso" && blnVersoCoro == false){
            passVerso = true
            //Verso.init(verso: "Toda a terra!", voz: "Contralto"),
            if attributeDict["voz"] == nil {
                currentElementVerso += "Verso.init(verso: \""
            } else {
                currentElementVerso += "Verso.init(verso: \""
                strVoz = attributeDict["voz"]!
            }
        }
        
        if(elementName=="coro"){
            passCoro = true
            currentElementVersoCoro = ""
            blnVersoCoro = true
        }
        if(elementName=="verso" && blnVersoCoro){
            passVersoCoro = true
            currentElementVersoCoro += "Verso.init(verso: \""
//            currentElementVersoCoro += "\""
        }

        if(elementName=="secao"){
            passSecao = true
        }
        if(elementName=="assunto"){
            passAssunto = true
        }
        
        
        
        if(elementName=="titulo_original"){
            passTitulo_original=true;
        }
        if(elementName=="metrica"){
            passMetrica=true;
        }
        if(elementName=="origem_letra"){
            passOrigem_letra=true;
        }
        if(elementName=="primeiro_verso_original"){
            passPrimeiro_verso_original=true;
        }
        if(elementName=="referencia_biblica"){
            passReferencia_biblica=true;
//            currentElementReferencia = ""
        }
//        else {
//            currentElementReferencia = nil
//        }
        if(elementName=="origem_musica"){
            passOrigem_musica=true;
        }
    }

    // 2
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if(elementName=="hino"){
            passHino = false
            dictHino.removeAll()
        }
        
        
        
        if(elementName=="numero"){
            passNumero = false
        }
        if(elementName=="titulo"){
            passTitulo = false
        }
        
        if(elementName=="estrofe"){
            passEstrofe = false
            currentElementEstrofeFim += currentElementEstrofe + currentElementVerso + "]),\n"
        }
        if(elementName=="verso" && blnVersoCoro == false){
            passVerso = false
            blnPassouUmaVeznoFoundCharacters = false
            //Verso.init(verso: "O Senhor está no seu santo templo,"),
            if strVoz == "" {
                currentElementVerso += "\"),\n"
            } else {
                currentElementVerso += "\", voz: \"" + strVoz + "\"),\n"
            }
            strVoz = ""
        }
        if(elementName=="coro"){
            passCoro = false
            currentElementCoroFim += currentElementCoro + currentElementVersoCoro + "]),\n"
        }
        if(elementName=="verso" && blnVersoCoro){
            passVersoCoro = false
            blnPassouUmaVeznoFoundCharacters = false
            if strVoz == "" {
                currentElementVersoCoro += "\"),\n"
            } else {
                currentElementVersoCoro += "\", voz: \"" + strVoz + "\"),\n"
            }
            strVoz = ""
        }
        
        if(elementName=="secao"){
            passSecao = false
        }
        if(elementName=="assunto"){
            passAssunto = false
        }
        
        
        
        if(elementName=="titulo_original"){
            passTitulo_original=false
        }
        if(elementName=="metrica"){
            passMetrica=false
        }
        if(elementName=="origem_letra"){
            passOrigem_letra=false
            blnPassouUmaVeznoFoundCharacters = false
            currentElementOrigem += "\"),\n"
        }

        if(elementName=="primeiro_verso_original"){
            passPrimeiro_verso_original=false
        }
        if(elementName=="referencia_biblica"){
            passReferencia_biblica=false
            blnPassouUmaVeznoFoundCharacters = false
            currentElementReferencia += "\",\n"
//            currentElementOrigem += "\"),\n"
        }
        if(elementName=="origem_musica"){
            passOrigem_musica=false
            blnPassouUmaVeznoFoundCharacters = false
            currentElementOrigemM += "\"),\n"
        }
        
    }

    // 3
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if passHino {

            if passNumero {
                currentElementNumero = string
            }
            if passTitulo {
                currentElementTitulo += string
            }

            if passEstrofe {
                currentElementEstrofe = "Estrofe.init(verso: [\n"
            }
            if passVerso {
                //Verso.init(verso: "O Senhor está no seu santo templo,"),
                currentElementVerso += string
                blnPassouUmaVeznoFoundCharacters = true
            }
            
            if passCoro {
                currentElementCoro = "], coro: [Coro.init(verso: [\n"
            }
            if passVersoCoro {
                currentElementVersoCoro += string
                blnPassouUmaVeznoFoundCharacters = true
            }
            
            
            if passSecao {
                currentElementSecao += string
            }
            if passAssunto {
                currentElementAssunto += string
            }
            
            if passTitulo_original {
                currentElementTituloOriginal += string
            }
            if passMetrica {
                currentElementMetrica = string
            }
            if passOrigem_letra {
                if blnPassouUmaVeznoFoundCharacters {
                    currentElementOrigem += string
                } else {
                    currentElementOrigem += "Autor.init(nome: \"" + string
                }
                blnPassouUmaVeznoFoundCharacters = true
            }
            if passPrimeiro_verso_original {
                currentElementPrimeiro += string
            }
            if passReferencia_biblica {
//                currentElementReferencia += "\"" + string + "\",\n"
                
                if blnPassouUmaVeznoFoundCharacters {
                    currentElementReferencia += string
                } else {
//                    currentElementOrigem += "Autor.init(nome: \"" + string
                    currentElementReferencia += "\"" + string
                }
                blnPassouUmaVeznoFoundCharacters = true
            }
            if passOrigem_musica {
                if blnPassouUmaVeznoFoundCharacters {
                    currentElementOrigemM += string
                } else {
                    currentElementOrigemM += "OrigemMusica.init(nome: \"" + string
                }
                
                blnPassouUmaVeznoFoundCharacters = true
            }
            
        }

    }


    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("failure error: parser")
    }



}

extension String {
    func leftPadding(toLength: Int, withPad character: Character) -> String {
        let stringLength = self.count
            return String(repeatElement(character, count: toLength - stringLength)) + self
    }
}

let oXMLParserHelper = XMLParserHelper()

for nHino in 301...400 {

    oXMLParserHelper.chamandoUrlXml(numero: nHino.description.leftPadding(toLength: 3, withPad: "0"))

    if currentElementNumero != nil {
        if currentElementNumero != "" {
            
            
            //var str = "Hino(numero: \"" + (currentElementNumero ?? "nada") + "\","
            var str = "Hino(numero: \"" + (currentElementNumero!) + "\",\n"
            str += "titulo: \"" + (currentElementTitulo) + "\",\n"
            str += "letraMusica: TextoHino.init(estrofe: [\n"


            str += currentElementEstrofeFim

            str += currentElementCoroFim
            str += "]),\n"
            str += "autores: [\n" + (currentElementOrigem) + "],\n"


            str += "secao: \"" + (currentElementSecao) + "\",\n"
            str += "assunto: \"" + (currentElementAssunto) + "\",\n"
            str += "titulo_original: \"" + (currentElementTituloOriginal) + "\",\n"
            str += "metrica: \"" + (currentElementMetrica!) + "\",\n"

            str += "primeiro_verso_original: \"" + (currentElementPrimeiro) + "\",\n"
            
            str += "referencia_biblica: [\n" + (currentElementReferencia) + "],\n"

            str += "origem_musica: [\n" + (currentElementOrigemM) + "]\n"
            str += "),\n"

            print("\n\n\n\n")
            print(str)


            
            
            currentElementNumero = ""
            currentElementTitulo = ""
            currentElementSecao = ""
            currentElementAssunto = ""
            currentElementMetrica = ""
            currentElementLetra = ""
            currentElementEstrofe = ""
            currentElementEstrofeFim = ""
            currentElementVerso = ""
            currentElementCoro = ""
            currentElementCoroFim = ""
            currentElementVersoCoro = ""


            currentElementOrigem = ""
            currentElementPrimeiro = ""
            currentElementTituloOriginal = ""
            currentElementReferencia = ""
            currentElementOrigemM = ""

            blnPassouUmaVeznoFoundCharacters = false
            
            
            
        }
    }
    


    
    
    
    
    
}

