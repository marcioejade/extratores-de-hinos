import UIKit
import Foundation

var str = ""
var strCoro = ""

class XMLParserHelper: NSObject, XMLParserDelegate {
    
    var currentElementNumero = ""
    var currentElementTitulo = ""

    var passRow = false
    var passTitle = false
    var passVerse = false
    var passCoro = false
    
    var strSecao = ""
    var strAssunto = ""
    
    
    func chamandoUrlXml() {
        if let path = Bundle.main.url(forResource: "SQLite", withExtension: "xml") {
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
    }

    // 1
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

        if(elementName=="row"){
            passRow = true
        }
        if(elementName=="title"){
            passTitle = true
            str += "Hino(numero: \""
        }
        if(elementName=="verse"){
            if attributeDict["type"] == "v" {
                passVerse = true
            } else if attributeDict["type"] == "c" {
                passCoro = true
            }
        }

//        if(elementName=="verso" && blnVersoCoro == false){
//            passVerso = true
//            //Verso.init(verso: "Toda a terra!", voz: "Contralto"),
//            if attributeDict["voz"] == nil {
//                currentElementVerso += "Verso.init(verso: \""
//            } else {
//                currentElementVerso += "Verso.init(verso: \""
//                strVoz = attributeDict["voz"]!
//            }
//        }
        
        
    }

    // 2
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if(elementName=="row"){
            passRow = false
            str += strCoro
            strCoro = ""
            str += "]),\n"
            
            str += "autores: [\n"
            str += "Autor.init(nome: \"(currentElementOrigem)\"),\n"
            str += "],\n"

            str += "secao: \"" + strSecao + "\",\n"
            str += "assunto: \"" + strAssunto + "\",\n"
            
            str += "origem_musica: [\n" + "" + "]\n"
            str += "),\n"
            
            currentElementNumero = ""
            currentElementTitulo = ""
        }
        if(elementName=="title"){
            passTitle = false
            
            //pega exatamente o número e o título
            let strNumero = currentElementNumero[4 ..< 7]
            let strTitulo = currentElementNumero.substring(fromIndex: 10)
            
            str += strNumero
            str += "\",\n"
            
            str += "titulo: \""
            str += strTitulo
            str += "\",\n"
            
            str += "letraMusica: TextoHino.init(estrofe: [\n"
            
            
            if (Int(strNumero)! >= 49) && (Int(strNumero)! <= 54) {
                strSecao = "Louvor e Adoração"
                strAssunto = "Deus Vencedor"
            }
            else if (Int(strNumero)! >= 55) && (Int(strNumero)! <= 65) {
                strSecao = "Louvor e Adoração"
                strAssunto = "Gratidão"
            }
            else if (Int(strNumero)! >= 66) && (Int(strNumero)! <= 76) {
                strSecao = "Confissão"
                strAssunto = "Contrição e Arrependimento"
            }
            else if (Int(strNumero)! >= 77) && (Int(strNumero)! <= 80) {
                strSecao = "Confissão"
                strAssunto = "Perdão"
            }
            else if (Int(strNumero)! >= 81) && (Int(strNumero)! <= 84) {
                strSecao = "Edificação"
                strAssunto = "Espírito Instruidor"
            }
            else if (Int(strNumero)! >= 85) && (Int(strNumero)! <= 85) {
                strSecao = "Edificação"
                strAssunto = "Espírito Consolador"
            }
            else if (Int(strNumero)! >= 86) && (Int(strNumero)! <= 86) {
                strSecao = "Edificação"
                strAssunto = "Espírito Vivificador"
            }
            else if (Int(strNumero)! >= 87) && (Int(strNumero)! <= 90) {
                strSecao = "Edificação"
                strAssunto = "Amor de Deus"
            }
            else if (Int(strNumero)! >= 91) && (Int(strNumero)! <= 93) {
                strSecao = "Edificação"
                strAssunto = "Fé"
            }
            else if (Int(strNumero)! >= 94) && (Int(strNumero)! <= 98) {
                strSecao = "Edificação"
                strAssunto = "Salvação"
            }
            else if (Int(strNumero)! >= 99) && (Int(strNumero)! <= 106) {
                strSecao = "Edificação"
                strAssunto = "Testemunho"
            }
            else if (Int(strNumero)! >= 107) && (Int(strNumero)! <= 115) {
                strSecao = "Edificação"
                strAssunto = "Companhia do Senhor"
            }
            else if (Int(strNumero)! >= 116) && (Int(strNumero)! <= 126) {
                strSecao = "Edificação"
                strAssunto = "Aspiração"
            }
            else if (Int(strNumero)! >= 127) && (Int(strNumero)! <= 130) {
                strSecao = "Edificação"
                strAssunto = "Oração"
            }
            else if (Int(strNumero)! >= 131) && (Int(strNumero)! <= 135) {
                strSecao = "Edificação"
                strAssunto = "Santificação"
            }
            else if (Int(strNumero)! >= 136) && (Int(strNumero)! <= 155) {
                strSecao = "Edificação"
                strAssunto = "Proteção"
            }
            else if (Int(strNumero)! >= 156) && (Int(strNumero)! <= 174) {
                strSecao = "Edificação"
                strAssunto = "Confiança"
            }
            else if (Int(strNumero)! >= 175) && (Int(strNumero)! <= 177) {
                strSecao = "Edificação"
                strAssunto = "Fidelidade"
            }
            else if (Int(strNumero)! >= 178) && (Int(strNumero)! <= 183) {
                strSecao = "Edificação"
                strAssunto = "Fraternidade"
            }
            else if (Int(strNumero)! >= 184) && (Int(strNumero)! <= 191) {
                strSecao = "Edificação"
                strAssunto = "Esperança"
            }
            else if (Int(strNumero)! >= 192) && (Int(strNumero)! <= 196) {
                strSecao = "Edificação"
                strAssunto = "Lar Celestial"
            }
            else if (Int(strNumero)! >= 197) && (Int(strNumero)! <= 211) {
                strSecao = "Apelo"
                strAssunto = "Convite"
            }
            else if (Int(strNumero)! >= 212) && (Int(strNumero)! <= 216) {
                strSecao = "Apelo"
                strAssunto = "Decisão"
            }
            else if (Int(strNumero)! >= 217) && (Int(strNumero)! <= 218) {
                strSecao = "Consagração"
                strAssunto = "Submissão"
            }
            else if (Int(strNumero)! >= 219) && (Int(strNumero)! <= 225) {
                strSecao = "Consagração"
                strAssunto = "Dedicação"
            }
            else if (Int(strNumero)! >= 226) && (Int(strNumero)! <= 229) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Advento"
            }
            else if (Int(strNumero)! >= 230) && (Int(strNumero)! <= 249) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Natal"
            }
            else if (Int(strNumero)! >= 250) && (Int(strNumero)! <= 256) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Ministério"
            }
            else if (Int(strNumero)! >= 257) && (Int(strNumero)! <= 259) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Entrada Triunfal"
            }
            else if (Int(strNumero)! >= 260) && (Int(strNumero)! <= 270) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Paixão e Morte"
            }
            else if (Int(strNumero)! >= 271) && (Int(strNumero)! <= 279) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Ressurreição"
            }
            else if (Int(strNumero)! >= 280) && (Int(strNumero)! <= 281) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Ascensão"
            }
            else if (Int(strNumero)! >= 282) && (Int(strNumero)! <= 289) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "A Grande Comissão"
            }
            else if (Int(strNumero)! >= 290) && (Int(strNumero)! <= 297) {
                strSecao = "Cristo - Sua Vida"
                strAssunto = "Segunda Vinda"
            }
            else if (Int(strNumero)! >= 298) && (Int(strNumero)! <= 301) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Igreja Militante"
            }
            else if (Int(strNumero)! >= 302) && (Int(strNumero)! <= 310) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Evangelização"
            }
            else if (Int(strNumero)! >= 311) && (Int(strNumero)! <= 321) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Trabalho Cristão"
            }
            else if (Int(strNumero)! >= 322) && (Int(strNumero)! <= 325) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Sociedade Auxiliadora Feminina"
            }
            else if (Int(strNumero)! >= 326) && (Int(strNumero)! <= 326) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "União Presbiteriana de Homens"
            }
            else if (Int(strNumero)! >= 327) && (Int(strNumero)! <= 328) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Discipulado e Serviço"
            }
            else if (Int(strNumero)! >= 329) && (Int(strNumero)! <= 329) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Posse de Pastor"
            }
            else if (Int(strNumero)! >= 330) && (Int(strNumero)! <= 333) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Batismo"
            }
            else if (Int(strNumero)! >= 334) && (Int(strNumero)! <= 336) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Convertidos"
            }
            else if (Int(strNumero)! >= 337) && (Int(strNumero)! <= 339) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Profissão de Fé"
            }
            else if (Int(strNumero)! >= 340) && (Int(strNumero)! <= 346) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Ceia do Senhor"
            }
            else if (Int(strNumero)! >= 347) && (Int(strNumero)! <= 349) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Dia do Senhor"
            }
            else if (Int(strNumero)! >= 350) && (Int(strNumero)! <= 354) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Escola Dominical"
            }
            else if (Int(strNumero)! >= 355) && (Int(strNumero)! <= 367) {
                strSecao = "Igreja - Seu Ministério"
                strAssunto = "Crianças"
            }
            else if (Int(strNumero)! >= 368) && (Int(strNumero)! <= 368) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Despedida"
            }
            else if (Int(strNumero)! >= 369) && (Int(strNumero)! <= 372) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Bíblia"
            }
            else if (Int(strNumero)! >= 373) && (Int(strNumero)! <= 375) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Ano Novo"
            }
            else if (Int(strNumero)! >= 376) && (Int(strNumero)! <= 380) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Pátria"
            }
            else if (Int(strNumero)! >= 381) && (Int(strNumero)! <= 381) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Cidade"
            }
            else if (Int(strNumero)! >= 382) && (Int(strNumero)! <= 392) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Mocidade"
            }
            else if (Int(strNumero)! >= 393) && (Int(strNumero)! <= 394) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Casamento"
            }
            else if (Int(strNumero)! >= 395) && (Int(strNumero)! <= 395) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Lar Cristão"
            }
            else if (Int(strNumero)! >= 396) && (Int(strNumero)! <= 396) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Aniversário"
            }
            else if (Int(strNumero)! >= 397) && (Int(strNumero)! <= 397) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Mãe"
            }
            else if (Int(strNumero)! >= 398) && (Int(strNumero)! <= 400) {
                strSecao = "Assuntos Diversos"
                strAssunto = "Final de Culto"
            }
            else {
                strSecao = ""
                strAssunto = ""
            }
            
            
            
            
            //110A, 237A, 281A, 325A, 354A
        }
        if(elementName=="verse"){
            passVerse = false
            passCoro = false
        }

        
//        if(elementName=="estrofe"){
//            passEstrofe = false
//            currentElementEstrofeFim += currentElementEstrofe + currentElementVerso + "]),\n"
//        }
//        if(elementName=="verso" && blnVersoCoro == false){
//            passVerso = false
//            blnPassouUmaVeznoFoundCharacters = false
//            //Verso.init(verso: "O Senhor está no seu santo templo,"),
//            if strVoz == "" {
//                currentElementVerso += "\"),\n"
//            } else {
//                currentElementVerso += "\", voz: \"" + strVoz + "\"),\n"
//            }
//            strVoz = ""
//        }
        
    }

    // 3
    func parser(_ parser: XMLParser, foundCharacters string: String) {

        if passRow {

        }

        if passTitle {
            currentElementNumero += string
//            currentElementTitulo += string
            
//            str += string
//            str += "titulo: \"" + (currentElementTitulo) + "\",\n"
        }
        if passVerse {
            
            str += "Estrofe.init(verso: [\n"

            let strTirandoEnter = string.replacingOccurrences(of: "\r\n", with: "+", options: .literal, range: nil)
            let arrVerso = strTirandoEnter.split(separator: "+")
            
            for verso in arrVerso {
                str += "Verso.init(verso: \""
                str += verso
                str += "\"),\n"
            }

            str += "]),\n"
        }
        
        if passCoro {
            
            strCoro += "], coro: [Coro.init(verso: [\n"

            let strTirandoEnter = string.replacingOccurrences(of: "\r\n", with: "+", options: .literal, range: nil)
            let arrVerso = strTirandoEnter.split(separator: "+")
            
            for verso in arrVerso {
                strCoro += "Verso.init(verso: \""
                strCoro += verso
                strCoro += "\"),\n"
            }

            strCoro += "]),\n"
//            print("passCoro \(strCoro)")
        }
        
        //colocar passCoro e fechar ] - ver lógica
        
        


//            if passEstrofe {
//                currentElementEstrofe = "Estrofe.init(verso: [\n"
//            }
//            if passVerso {
//                //Verso.init(verso: "O Senhor está no seu santo templo,"),
//                currentElementVerso += string
//                blnPassouUmaVeznoFoundCharacters = true
//            }
//
//            if passCoro {
//                currentElementCoro = "], coro: [Coro.init(verso: [\n"
//            }
//            if passVersoCoro {
//                currentElementVersoCoro += string
//                blnPassouUmaVeznoFoundCharacters = true
//            }


    }


    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("failure error: parser")
    }



}

extension String {
    func leftPadding(toLength: Int, withPad character: Character) -> String {
        let stringLength = self.count
            return String(repeatElement(character, count: toLength - stringLength)) + self
    }
    
    

    var length: Int {
        return count
    }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }

    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }

    
}

let oXMLParserHelper = XMLParserHelper()

//for nHino in 301...400 {

oXMLParserHelper.chamandoUrlXml()

print("\n\n\n\n")
print(str)


